﻿# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: picker_ui.py
Date: 2018.12.29
Revision: 2020.01.26
Copyright: Copyright 2018 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2018, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


import sys
import os
from collections import deque
import json
import colorsys

from PySide2 import QtWidgets, QtCore, QtGui, QtUiTools


# -------------------------------- CONSTANTS -------------------------------- #

GENERIC = 'GENERIC'
MAYA = 'maya'
NUKE = 'nuke'
HOUDINI = 'houdini'


# -------------------------------- SOFTWARE -------------------------------- #

software = 'generic'
if MAYA in sys.executable.lower():
    software = MAYA
    import external_maya_commands

elif NUKE in sys.executable.lower():
    software = NUKE
    import external_nuke_commands


# -------------------------------- WIDGET -------------------------------- #

class UniversalPicker(QtWidgets.QWidget):

    def __init__(self):

        self.SOFTWARE = software

        # CONTROLLER
        self.controller = UniversalPickerController(self.SOFTWARE)

        # WIDGET LOAD
        QtWidgets.QWidget.__init__(self)
        path = os.path.abspath(__file__)
        dir_path = os.path.dirname(path).replace('\\', '/')
        file = QtCore.QFile(dir_path+'/universal_picker.ui')
        file.open(QtCore.QFile.ReadOnly)
        loader = QtUiTools.QUiLoader()
        self.ui = loader.load(file,self)

        # ICONS AND PANTONES DIRECTORY
        self.icons_dir_path = dir_path + '/icons/'
        self.pantones_dir_path =  dir_path + '/pantones/'

        # MAIN CONFIG OF THE WINDOW
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowTitle('Universal color picker')
        self.setFixedWidth(442)
        self.setFixedHeight(854)
        self.setWindowIcon(QtGui.QIcon(self.icons_dir_path + 'picker.png'))

        # RGBA WIDGETS
        self.rgba_sliders = [self.ui.rgb_slider_red, self.ui.rgb_slider_green,
                             self.ui.rgb_slider_blue, self.ui.rgb_slider_alpha]
        self.rgba_doubles = [self.ui.rgb_double_red, self.ui.rgb_double_green,
                             self.ui.rgb_double_blue, self.ui.rgb_double_alpha]
        self.rgba_ints = [self.ui.rgb_int_red, self.ui.rgb_int_green, self.ui.rgb_int_blue]

        # HSV WIDGETS
        self.hsv_sliders = [self.ui.hsv_slider_hue, self.ui.hsv_slider_sat, self.ui.hsv_slider_val]
        self.hsv_doubles = [self.ui.hsv_double_hue, self.ui.hsv_double_sat, self.ui.hsv_double_val]
        
        # COLOR MAIN INFO
        self.color_dict = {'r' : 0, 'g' : 0, 'b' : 0, 'there_is_alpha' : False, 'a' : 0}

        # COMMANDS HISTORY
        self.commands_history = []
        self.commands_history_d = deque()

        # CONNECTIONS, VALIDATORS AND RUNNING
        self.establish_software()
        self.make_connections()
        self.set_validators()
        self.load_pantones()
        self.run()

    # SETUP
    def establish_software(self):
        
        # ICONS FOR THE SPECIFIC BUTTONS
        icon_create = QtGui.QIcon(self.icons_dir_path + 'create.png')
        icon_paint = QtGui.QIcon(self.icons_dir_path + 'paint.png')

        # ESTABLISHING SPECIFICS
        if self.SOFTWARE == 'maya':
            self.ui.specifics_label.setText('MAYA')

            self.ui.specifics_button_0.setIcon(icon_create)
            self.ui.specifics_button_0.setText('Lambert')
            self.ui.specifics_button_0.setToolTip('Create a Lambert shader with the current selected color')

            self.ui.specifics_button_1.setIcon(icon_create)
            self.ui.specifics_button_1.setText('aiStandard')
            self.ui.specifics_button_1.setToolTip('Create an aiStandardSurface shader with the current selected color')

            self.ui.specifics_button_2.setIcon(icon_create)
            self.ui.specifics_button_2.setText('Constant')
            self.ui.specifics_button_2.setToolTip('Create a colorConstant node')

            self.ui.specifics_set_label.setText('Set attribute')
            self.ui.specifics_set_input.setText('inColor')

            self.ui.specifics_get_label.setText('Get attribute')
            self.ui.specifics_get_input.setText('color')

        elif self.SOFTWARE == 'nuke':
            self.ui.specifics_label.setText('NUKE')

            self.ui.specifics_button_0.setIcon(icon_paint)
            self.ui.specifics_button_0.setText('Paint sel.')
            self.ui.specifics_button_0.setToolTip('Paint selected nodes with the current selected color')

            self.ui.specifics_button_1.setIcon(icon_create)
            self.ui.specifics_button_1.setText('Constant')
            self.ui.specifics_button_1.setToolTip('Create a Constant node')

            self.ui.specifics_button_2.setIcon(icon_create)
            self.ui.specifics_button_2.setText('Backdrop')
            self.ui.specifics_button_2.setToolTip('Create Backdrop around selected nodes')

            self.ui.specifics_set_label.setText('Set knob value')
            self.ui.specifics_set_input.setText('color')

            self.ui.specifics_get_label.setText('Get knob value')
            self.ui.specifics_get_input.setText('tile_color')

        elif self.SOFTWARE == 'generic':

            self.setFixedWidth(442)
            self.setFixedHeight(660)

            self.ui.specifics_divider.setVisible(False)
            self.ui.specifics_label.setVisible(False)
            self.ui.specifics_frame.setVisible(False)

            self.ui.info_button.move(400, 630)
            self.ui.developer_info_label.move(20, 630)

    def make_connections(self):

        # CONNECTIONS FOR RGBA
        self.ui.rgb_double_red.valueChanged.connect(
            lambda: self.ui.rgb_slider_red.setSliderPosition(self.ui.rgb_double_red.value() * 10000))
        self.ui.rgb_double_green.valueChanged.connect(
            lambda: self.ui.rgb_slider_green.setSliderPosition(self.ui.rgb_double_green.value() * 10000))
        self.ui.rgb_double_blue.valueChanged.connect(
            lambda: self.ui.rgb_slider_blue.setSliderPosition(self.ui.rgb_double_blue.value() * 10000))
        self.ui.rgb_double_alpha.valueChanged.connect(
            lambda: self.ui.rgb_slider_alpha.setSliderPosition(self.ui.rgb_double_alpha.value() * 10000))
            
        for double_w in self.rgba_doubles:
            double_w.valueChanged.connect(self.update_rgba_feedback)
            double_w.valueChanged.connect(self.rgb_to_hsv)
            double_w.valueChanged.connect(self.rgb_to_hex)
            double_w.valueChanged.connect(self.rgb_to_cmyk)
            double_w.valueChanged.connect(self.update_main_feedback)

        self.ui.rgb_slider_red.valueChanged.connect(
            lambda: self.ui.rgb_double_red.setValue(float(self.ui.rgb_slider_red.value()) / 10000))
        self.ui.rgb_slider_green.valueChanged.connect(
            lambda: self.ui.rgb_double_green.setValue(float(self.ui.rgb_slider_green.value()) / 10000))
        self.ui.rgb_slider_blue.valueChanged.connect(
            lambda: self.ui.rgb_double_blue.setValue(float(self.ui.rgb_slider_blue.value()) / 10000))
        self.ui.rgb_slider_alpha.valueChanged.connect(
            lambda: self.ui.rgb_double_alpha.setValue(float(self.ui.rgb_slider_alpha.value()) / 10000))
 
 
        # ALPHA CHECKBOX
        self.ui.rgb_checkbox_alpha.clicked.connect(self.toggle_alpha)

        # CONNECTIONS FOR HSV
        self.ui.hsv_double_hue.valueChanged.connect(
            lambda: self.ui.hsv_slider_hue.setSliderPosition(self.ui.hsv_double_hue.value() * 10000))
        self.ui.hsv_double_val.valueChanged.connect(
            lambda: self.ui.hsv_slider_val.setSliderPosition(self.ui.hsv_double_val.value() * 10000))
        self.ui.hsv_double_sat.valueChanged.connect(
            lambda: self.ui.hsv_slider_sat.setSliderPosition(self.ui.hsv_double_sat.value() * 10000))

        for double_w in self.hsv_doubles:
            double_w.valueChanged.connect(self.update_hsv_feedback)
            double_w.valueChanged.connect(self.hsv_to_rgb)

        self.ui.hsv_slider_hue.valueChanged.connect(
            lambda: self.ui.hsv_double_hue.setValue(float(self.ui.hsv_slider_hue.value()) / 10000))
        self.ui.hsv_slider_val.valueChanged.connect(
            lambda: self.ui.hsv_double_val.setValue(float(self.ui.hsv_slider_val.value()) / 10000))
        self.ui.hsv_slider_sat.valueChanged.connect(
            lambda: self.ui.hsv_double_sat.setValue(float(self.ui.hsv_slider_sat.value()) / 10000))

        # CONNECTIONS FOR HEX
        self.ui.hex_text.returnPressed.connect(self.hex_to_rgb)

        # CONNECTIONS FOR PANTONE
        self.ui.pantones_checkbox_1.clicked.connect(lambda: self.toggle_pantones(0))
        self.ui.pantones_checkbox_2.clicked.connect(lambda: self.toggle_pantones(1))
        self.ui.pantones_checkbox_3.clicked.connect(lambda: self.toggle_pantones(2))

        self.ui.pantones_coated.currentIndexChanged.connect(lambda: self.update_pantone_feedback(0))
        self.ui.pantones_pastels_neons.currentIndexChanged.connect(lambda: self.update_pantone_feedback(1))
        self.ui.pantones_fashion.currentIndexChanged.connect(lambda: self.update_pantone_feedback(2))

        # CONNECTIONS FOR BUTTONS
        self.ui.specifics_button_0.clicked.connect(lambda: self.to_external_commands(0))
        self.ui.specifics_button_1.clicked.connect(lambda: self.to_external_commands(1))
        self.ui.specifics_button_2.clicked.connect(lambda: self.to_external_commands(2))
        self.ui.specifics_set_button.clicked.connect(self.to_external_set)
        self.ui.specifics_get_button.clicked.connect(self.to_external_get)

        # INPUT/OUTPUT
        self.ui.specifics_io.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        clear_action = QtWidgets.QAction('CLEAR', self.ui.specifics_io)
        clear_action.triggered.connect(self.clear_io)
        self.ui.specifics_io.addAction(clear_action)
        previous_action = QtWidgets.QAction('PREVIOUS', self.ui.specifics_io)
        previous_action.triggered.connect(self.get_last_command)
        self.ui.specifics_io.addAction(previous_action)
        next_action = QtWidgets.QAction('NEXT', self.ui.specifics_io)
        next_action.triggered.connect(self.get_next_command)
        self.ui.specifics_io.addAction(next_action)
        self.ui.specifics_io.textEdited.connect(lambda: self.ui.specifics_io.setStyleSheet('QLineEdit{color:none;}'))
        self.ui.specifics_io.returnPressed.connect(self.to_external_IO)

        # CONNECTION FOR INFO BUTTON
        self.ui.info_button.clicked.connect(self.open_web)

    def set_validators(self):
        hex_re = QtCore.QRegExp("#?[0-9a-fA-F]{6}")
        hex_validator = QtGui.QRegExpValidator(hex_re)
        self.ui.hex_text.setValidator(hex_validator)

    # FEEDBACK AND UI UPDATE
    def update_color_dict(self):
        self.color_dict['r'] = self.ui.rgb_double_red.value()
        self.color_dict['g'] = self.ui.rgb_double_green.value()

        self.color_dict['b'] = self.ui.rgb_double_blue.value()
        self.color_dict['there_is_alpha'] = self.ui.rgb_checkbox_alpha.isChecked()
        self.color_dict['a'] = self.ui.rgb_double_alpha.value()

    def update_rgba_feedback(self):

        # Setting values
        r, g, b, a = int(round(self.ui.rgb_double_red.value()*255)), int(round(self.ui.rgb_double_green.value()*255)),\
                     int(round(self.ui.rgb_double_blue.value()*255)) , int(round(self.ui.rgb_double_alpha.value()*255))

        self.ui.rgb_int_red.setValue(r)
        self.ui.rgb_int_green.setValue(g)
        self.ui.rgb_int_blue.setValue(b)

        if not self.ui.rgb_checkbox_alpha.isChecked():
            a = ''
            thereisA = ''
        else:
            a = ',' + str(a)
            thereisA = 'a'

        self.ui.rgb_text_color.setText('rgb{0}({1},{2},{3}{4})'.format(thereisA, r, g, b, a))

    def toggle_alpha(self):

        if self.ui.rgb_checkbox_alpha.isChecked():
            self.ui.rgb_slider_alpha.setEnabled(True)
            self.ui.rgb_slider_alpha.setStyleSheet('QSlider::groove:horizontal'
                                                '{background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0,'
                                                'stop:0 rgb(0, 0, 0),'
                                                'stop:1 rgb(255, 255,255));'
                                                'height: 5px;'
                                                'border-radius: 2px;}'
                                                'QSlider::handle:horizontal'
                                                '{background:rgb(200,200,200);'
                                                'width: 8px;margin-top: -6px;margin-bottom: -6px;'
                                                'border-radius: 2px;}')

            self.ui.rgb_double_alpha.setEnabled(True)

        else:
            self.ui.rgb_slider_alpha.setDisabled(True)
            self.ui.rgb_slider_alpha.setStyleSheet('QSlider::groove:horizontal'
                                                   '{background:none;'
                                                   'border:1px dotted gray;'
                                                   'height:5px;'
                                                   'border-radius: 2px;}'
                                                   'QSlider::handle:horizontal'
                                                   'background:none;'
                                                   'width: 8px;margin-top: -6px;margin-bottom: -6px;'
                                                   'border-radius: 2px;}')
            self.ui.rgb_double_alpha.setDisabled(True)


        self.update_rgba_feedback()
        self.update_main_feedback()

    def update_hsv_feedback(self):

        h, s, v = int(round(self.ui.hsv_double_hue.value()*359)), int(round(self.ui.hsv_double_sat.value()*100)),\
                  int(round(self.ui.hsv_double_val.value()*100))

        self.ui.hsv_int_hue.setValue(h)
        self.ui.hsv_int_sat.setValue(s)
        self.ui.hsv_int_val.setValue(v)

        self.ui.hsv_slider_sat.setStyleSheet('QSlider::groove:horizontal'
                                             '{{background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0,'
                                             'stop:0 hsv(180,0,125),'
                                             'stop:1 hsv({},255,255));'
                                             'height: 5px; border-radius: 2px;}}'
                                             'QSlider::handle:horizontal'
                                             '{{background:rgb(200,200,200); width: 8px;'
                                             'margin-top: -6px; margin-bottom: -6px; border-radius: 2px;}}'.format(h))

        self.ui.hsv_text_color.setText('hsv({0},{1},{2})'.format(h,
                                                                 int(round(self.ui.hsv_double_sat.value()*255)),
                                                                 int(round(self.ui.hsv_double_val.value()*255))))
        
    def update_main_feedback(self):

        r, g, b, a = self.ui.rgb_int_red.value(), self.ui.rgb_int_green.value(),\
                     self.ui.rgb_int_blue.value(), int(round(self.ui.rgb_double_alpha.value()*255))

        if a == 1: a = 0

        h, s, v = self.ui.hsv_int_hue.value(), self.ui.hsv_int_sat.value(), self.ui.hsv_int_val.value()

        if not self.ui.rgb_checkbox_alpha.isChecked():
            a = ''
            thereisA = ''
        else:
            a = ',' + str(a)
            thereisA = 'a'

        self.ui.main_color_label.setStyleSheet('background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0,'
                                               'stop:0.49 rgb{isA}({R},{G},{B},255),'
                                               'stop:0.5 rgb{isA}({R},{G},{B}{A}),'
                                               'stop:1 rgb{isA}({R},{G},{B}{A}));'
                                               'border-radius:10px;'.format(isA=thereisA, R=r, G=g, B=b, A=a))

        main_feedback_text = '<html><head/><body><p><span style=" text-decoration: underline;">' \
                             'COLOR PROPERTIES<br/></span>R: {R} / G: {G} / B: {B}' \
                             '<br/> </span> H: {H}º / S: {S}% / V: {V}% < br / > ' \
                             'Hex: {HEX}</p></body></html>' \
                             ''.format(R=r, G=g, B=b, H=h, S=s, V=v, HEX=self.ui.hex_text.text())

        self.ui.main_info_label.setText(main_feedback_text)

    # COLOR CONVERSIONS
    def rgb_to_hsv(self):
        for double_w in self.hsv_doubles:
            double_w.valueChanged.disconnect(self.hsv_to_rgb)

        r, g, b = self.rgba_doubles[0].value(), self.rgba_doubles[1].value(), self.rgba_doubles[2].value()

        hsv_from_rgb = colorsys.rgb_to_hsv(r,g,b)
        
        for i in range(3):
            self.hsv_doubles[i].setValue(hsv_from_rgb[i])

        for double_w in self.hsv_doubles:
            double_w.valueChanged.connect(self.hsv_to_rgb)

    def hsv_to_rgb(self):
        for double_w in self.rgba_doubles:
            double_w.valueChanged.disconnect(self.rgb_to_hsv)

        h, s, v = self.hsv_doubles[0].value(), self.hsv_doubles[1].value(), self.hsv_doubles[2].value()

        rgb_from_hsv = colorsys.hsv_to_rgb(h, s, v)

        for i in range(3):
            self.rgba_doubles[i].setValue(rgb_from_hsv[i])

        for double_w in self.rgba_doubles:
            double_w.valueChanged.connect(self.rgb_to_hsv)

    def rgb_to_hex(self):
        r, g, b = self.rgba_ints[0].value(), self.rgba_ints[1].value(), self.rgba_ints[2].value()

        hex_from_rgb = '#' + hex(r)[2:].zfill(2) + hex(g)[2:].zfill(2) + hex(b)[2:].zfill(2)

        self.ui.hex_text.setText(hex_from_rgb)

    def hex_to_rgb(self):
        for double_w in self.rgba_doubles:
            double_w.valueChanged.disconnect(self.rgb_to_hex)

        hex = self.ui.hex_text.text()

        if '#' in hex:
            hex = hex[1:]
        else:
            self.ui.hex_text.setText("#" + hex)

        rgb_from_hex = [int(hex[0:2], 16), int(hex[2:4], 16), int(hex[4:6], 16)]

        for i in range(3):
            self.rgba_doubles[i].setValue(float(rgb_from_hex[i])/255)

        for double_w in self.rgba_doubles:
            double_w.valueChanged.connect(self.rgb_to_hex)

    def rgb_to_cmyk(self):
        r, g, b = self.rgba_doubles[0].value(), self.rgba_doubles[1].value(), self.rgba_doubles[2].value()

        c = 1 - r
        m = 1 - g
        y = 1 - b

        temp_k = 1
        if c < temp_k:
            temp_k = c
        if m < temp_k:
            temp_k = m
        if y < temp_k:
            temp_k = y
        if temp_k == 1:
            c = 0
            m = 0
            y = 0
        else:
            c = (c - temp_k) / (1 - temp_k)
            m = (m - temp_k) / (1 - temp_k)
            y = (y - temp_k) / (1 - temp_k)

        k = temp_k

        c, m, y, k = int(c*255), int(m*255), int(y*255), int(k*255)

        self.ui.cmyk_swatch_cyan.setStyleSheet('background: hsv(180,255,255,{})'.format(c))
        self.ui.cmyk_swatch_magenta.setStyleSheet('background: hsv(299,255,255,{})'.format(m))
        self.ui.cmyk_swatch_yellow.setStyleSheet('background: hsv(60,255,255,{})'.format(y))
        self.ui.cmyk_swatch_black.setStyleSheet('background: hsv(0,0,0,{})'.format(k))

        self.ui.cmyk_label_cyan .setText(str(c))
        self.ui.cmyk_label_magenta.setText(str(m))
        self.ui.cmyk_label_yellow.setText(str(y))
        self.ui.cmyk_label_black.setText(str(k))

    # PANTONES
    def load_pantones(self):
        
        # Coated pantones
        coated_pantones_path = self.pantones_dir_path + 'pantone_coated.json'

        with open(coated_pantones_path, 'r') as coated_pantones_data:
            coated_pantones_dict = json.load(coated_pantones_data)

        for color in sorted(coated_pantones_dict.keys()):
            hex = coated_pantones_dict[color]
            rgb_from_hex = [int(hex[0:2], 16), int(hex[2:4], 16), int(hex[4:6], 16)]
            swatch = QtGui.QColor(rgb_from_hex[0], rgb_from_hex[1], rgb_from_hex[2])
            swatch_icon = QtGui.QPixmap(16, 9)
            swatch_icon.fill(swatch)

            name = color.capitalize()

            self.ui.pantones_coated.addItem(swatch_icon, name)
            
        # Pastel and neon pantones
        pastels_neons_pantones_path = self.pantones_dir_path + 'pantone_pastels_neons.json'

        with open(pastels_neons_pantones_path, 'r') as pastels_neons_pantones_data:
            pastels_neons_pantones_dict = json.load(pastels_neons_pantones_data)

        for color in sorted(pastels_neons_pantones_dict.keys()):
            hex = pastels_neons_pantones_dict[color]
            rgb_from_hex = [int(hex[0:2], 16), int(hex[2:4], 16), int(hex[4:6], 16)]
            swatch = QtGui.QColor(rgb_from_hex[0], rgb_from_hex[1], rgb_from_hex[2])
            swatch_icon = QtGui.QPixmap(16, 9)
            swatch_icon.fill(swatch)

            name = color.capitalize()

            self.ui.pantones_pastels_neons.addItem(swatch_icon, name)

        # Fashion pantones
        fashion_pantones_path = self.pantones_dir_path + 'pantone_fashion.json'

        with open(fashion_pantones_path, 'r') as fashion_pantones_data:
            fashion_pantones_dict = json.load(fashion_pantones_data)

        for color in sorted(fashion_pantones_dict):
            hex = fashion_pantones_dict[color]['hex']
            rgb_from_hex = [int(hex[0:2], 16), int(hex[2:4], 16), int(hex[4:6], 16)]
            swatch = QtGui.QColor(rgb_from_hex[0], rgb_from_hex[1], rgb_from_hex[2])
            swatch_icon = QtGui.QPixmap(16, 9)
            swatch_icon.fill(swatch)

            name = fashion_pantones_dict[color]['name'].capitalize() + ' | ' + color

            self.ui.pantones_fashion.addItem(swatch_icon, name)

    def toggle_pantones(self, index):

        if index == 0:

            if (not self.ui.pantones_checkbox_2.isChecked()) and (not self.ui.pantones_checkbox_3.isChecked()):
                self.ui.pantones_checkbox_1.setCheckState(QtCore.Qt.CheckState.Checked)
            else:
                self.ui.pantones_checkbox_2.setCheckState(QtCore.Qt.CheckState.Unchecked)
                self.ui.pantones_checkbox_3.setCheckState(QtCore.Qt.CheckState.Unchecked)

                self.ui.pantones_coated.setDisabled(False)
                self.ui.pantones_pastels_neons.setDisabled(True)
                self.ui.pantones_fashion.setDisabled(True)

                self.update_pantone_feedback(0)

        elif index == 1:

            if (not self.ui.pantones_checkbox_1.isChecked()) and (not self.ui.pantones_checkbox_3.isChecked()):
                self.ui.pantones_checkbox_2.setCheckState(QtCore.Qt.CheckState.Checked)
            else:
                self.ui.pantones_checkbox_1.setCheckState(QtCore.Qt.CheckState.Unchecked)
                self.ui.pantones_checkbox_3.setCheckState(QtCore.Qt.CheckState.Unchecked)

                self.ui.pantones_coated.setDisabled(True)
                self.ui.pantones_pastels_neons.setDisabled(False)
                self.ui.pantones_fashion.setDisabled(True)

                self.update_pantone_feedback(1)

        elif index == 2:

            if (not self.ui.pantones_checkbox_1.isChecked()) and (not self.ui.pantones_checkbox_2.isChecked()):
                self.ui.pantones_checkbox_3.setCheckState(QtCore.Qt.CheckState.Checked)
            else:
                self.ui.pantones_checkbox_1.setCheckState(QtCore.Qt.CheckState.Unchecked)
                self.ui.pantones_checkbox_2.setCheckState(QtCore.Qt.CheckState.Unchecked)

                self.ui.pantones_coated.setDisabled(True)
                self.ui.pantones_pastels_neons.setDisabled(True)
                self.ui.pantones_fashion.setDisabled(False)

                self.update_pantone_feedback(2)

    def update_pantone_feedback(self, index):

        def establish_neutral():
            self.ui.pantone_info_label.setText('')
            self.ui.pantones_swatch_label.setStyleSheet('border-radius:7px;')

            self.ui.rgb_double_red.setValue(0.5)
            self.ui.rgb_double_green.setValue(0.5)
            self.ui.rgb_double_blue.setValue(0.5)


        pantone_info_label_ = '<html><head/><body><p><span style=" font-size:9pt; text-decoration: underline;">' \
                              'SELECTED {COLOR}<br/></span><span style=" font-size:7pt;">' \
                              'RGB: {R} {G} {B}<br/>#{HEX}</span></p></body></html>'

        pantone_swatch_stylesheet = 'border-radius:7px; background-color: qlineargradient(' \
                                    'spread:pad, x1:0, y1:0, x2:1, y2:0,' \
                                    'stop:0 rgba({R}, {G}, {B}, 255),' \
                                    'stop:1 rgba({R}, {G}, {B}, 0));' \

        hex = ''

        if index == 0:

            if self.ui.pantones_coated.currentIndex() == 0:
                establish_neutral()
                return

            pantone_name = self.ui.pantones_coated.currentText().lower()

            coated_pantones_path = self.pantones_dir_path + 'pantone_coated.json'

            with open(coated_pantones_path, 'r') as coated_pantones_data:
                coated_pantones_dict = json.load(coated_pantones_data)

            for color in sorted(coated_pantones_dict.keys()):
                if color == pantone_name:
                    hex = coated_pantones_dict[color]
                    break

                else:
                    self.ui.pantone_info_label.setText('')
                    self.ui.pantones_swatch_label.setStyleSheet('border-radius:7px;')

        if index == 1:

            if self.ui.pantones_pastels_neons.currentIndex() == 0:
                establish_neutral()
                return

            pantone_name = self.ui.pantones_pastels_neons.currentText().lower()

            pastels_neons_pantones_path = self.pantones_dir_path + 'pantone_pastels_neons.json'

            with open(pastels_neons_pantones_path, 'r') as pastels_neons_pantones_data:
                pastels_neons_pantones_dict = json.load(pastels_neons_pantones_data)

            for color in sorted(pastels_neons_pantones_dict.keys()):
                if color == pantone_name:
                    hex = pastels_neons_pantones_dict[color]
                    break

                else:
                    self.ui.pantone_info_label.setText('')
                    self.ui.pantones_swatch_label.setStyleSheet('border-radius:7px;')
                    
        elif index == 2:

            if self.ui.pantones_fashion.currentIndex() == 0:
                establish_neutral()
                return

            pantone_name = self.ui.pantones_fashion.currentText().lower().split('|')[0][:-1]

            fashion_pantones_path = self.pantones_dir_path + 'pantone_fashion.json'

            with open(fashion_pantones_path, 'r') as fashion_pantones_data:
                fashion_pantones_dict = json.load(fashion_pantones_data)

            for color in sorted(fashion_pantones_dict.keys()):
                if fashion_pantones_dict[color]['name'] == pantone_name:
                    hex = fashion_pantones_dict[color]['hex']
                    break

                else:
                    self.ui.pantone_info_label.setText('')
                    self.ui.pantones_swatch_label.setStyleSheet('border-radius:7px;')

        if not hex:
            return

        # Hex to rgb and displaying the color
        rgb_from_hex = [int(hex[0:2], 16), int(hex[2:4], 16), int(hex[4:6], 16)]
        self.ui.pantone_info_label.setText(pantone_info_label_.format(
            COLOR=color, R=rgb_from_hex[0], G=rgb_from_hex[1], B=rgb_from_hex[2], HEX=hex))
        self.ui.pantones_swatch_label.setStyleSheet(pantone_swatch_stylesheet.format(
            R=rgb_from_hex[0], G=rgb_from_hex[1], B=rgb_from_hex[2]))

        self.ui.rgb_double_red.setValue(float(rgb_from_hex[0]) / 255)
        self.ui.rgb_double_green.setValue(float(rgb_from_hex[1]) / 255)
        self.ui.rgb_double_blue.setValue(float(rgb_from_hex[2]) / 255)
                    
    # EXTERNAL COMMANDS
    def to_external_commands(self, command_index):
        self.update_color_dict()

        if command_index == 0:
            completion, response = self.controller.external_action_0(self.color_dict)
            self.show_feedback(completion, response)
        elif command_index == 1:
            completion, response = self.controller.external_action_1(self.color_dict)
            self.show_feedback(completion, response)
        elif command_index == 2:
            completion, response = self.controller.external_action_2(self.color_dict)
            self.show_feedback(completion, response)
            
    def to_external_set(self):
        self.update_color_dict()

        completion, response = self.controller.external_set(self.ui.specifics_set_input.text(), self.color_dict)
        self.show_feedback(completion, response)
    
    def to_external_get(self):
        completion, response = self.controller.external_get(self.ui.specifics_get_input.text())
        self.show_feedback(completion, response)
    
    def to_external_IO(self):
        self.commands_history.append(self.ui.specifics_io.text())
        self.commands_history_d = deque(self.commands_history)
        completion, response = self.controller.external_IO(self.ui.specifics_io.text())
        self.show_feedback(completion, response)
                            
    # INPUT / OUTPUT LINE
    def show_feedback(self, completion, response):
        if completion:
            self.ui.specifics_io.setStyleSheet('QLineEdit{color: lime;}')
        else:
            print 'ERROR', response
            self.ui.specifics_io.setStyleSheet('QLineEdit{color: red;}')

        self.ui.specifics_io.setText(response)

    def clear_io(self):
        self.ui.specifics_io.clear()
        self.ui.specifics_io.setStyleSheet('QLineEdit{color:none}')

    def get_last_command(self):
        self.commands_history_d.rotate(1)
        self.ui.specifics_io.setText(self.commands_history_d[0])
        self.ui.specifics_io.setStyleSheet('QLineEdit{color:none;}')

    def get_next_command(self):
        self.commands_history_d.rotate(-1)
        self.ui.specifics_io.setText(self.commands_history_d[0])
        self.ui.specifics_io.setStyleSheet('QLineEdit{color:none;}')

    # UTILITY
    def open_web(self):
        import webbrowser
        webbrowser.open('www.jaimervq.com')

    # RUN AND SHOW THE WIDGET
    def run(self):
        self.show()


# -------------------------------- CONTROLLER -------------------------------- #

class UniversalPickerController(object):

    def __init__(self, software):

        self.SOFTWARE = software
        
    def external_action_0(self, color_dict):
        if self.SOFTWARE == MAYA:
            return external_maya_commands.maya_lambert(**color_dict)
        elif self.SOFTWARE == NUKE:
            return external_nuke_commands.nuke_paint_nodes(color_dict['r'], color_dict['g'], color_dict['b'])
        
    def external_action_1(self, color_dict):
        if self.SOFTWARE == MAYA:
            return external_maya_commands.maya_aiStandard(**color_dict)
        elif self.SOFTWARE == NUKE:
            return external_nuke_commands.nuke_constant(**color_dict)
        
    def external_action_2(self, color_dict):
        if self.SOFTWARE == MAYA:
            return external_maya_commands.maya_constant(**color_dict)
        elif self.SOFTWARE == NUKE:
            return external_nuke_commands.nuke_backdrop(color_dict['r'], color_dict['g'], color_dict['b'])
        
    def external_set(self, attribute, color_dict):
        if self.SOFTWARE == MAYA:
            return external_maya_commands.maya_set(attribute, color_dict['r'], color_dict['g'], color_dict['b'])
        elif self.SOFTWARE == NUKE:
            return external_nuke_commands.nuke_set(attribute, **color_dict)
        
    def external_get(self, attribute):
        if self.SOFTWARE == MAYA:
            return external_maya_commands.maya_get(attribute)
        elif self.SOFTWARE == NUKE:
            return external_nuke_commands.nuke_get(attribute)
        
    def external_IO(self, command):
        if self.SOFTWARE == MAYA:
            return external_maya_commands.maya_IO(command)
        elif self.SOFTWARE == NUKE:
            return external_nuke_commands.nuke_IO(command)
    